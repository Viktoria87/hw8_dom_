let data = [
    {num: "1", currency: "AUD", saleRateNB: "12.83", purchaseRateNB: "12.83"},
    {num: "2", currency: "CAD", saleRateNB: "13.21 ", purchaseRateNB: "13.21"},
    {num: "3", currency: "CZK", saleRateNB: "0.67", purchaseRateNB: "0.67"},
    {num: "4", currency: "DKK", saleRateNB: "2.52 ", purchaseRateNB: "2.52"}
];


let table = document.createElement('table');
document.getElementById('root').appendChild(table);

data.forEach((item, index) => {
    let row = document.createElement('tr');
    table.append(row);
    let heading = document.createElement('th');
    for (let key in item) {
         if (index === data.heading){
            
            heading.innerHTML = `${key}`;
            //row.append(heading);
            //heading.appendChild(row);
            
        } else {
            let rowData = document.createElement('td');
            rowData.innerHTML = `${item[key]}`;
            row.append(rowData);
            
        }

    }
});



//вариант работает с библиотекой ajax
/*let data = [
    {num: "1", currency: "AUD", saleRateNB: "12.83", purchaseRateNB: "12.83"},
    {num: "2", currency: "CAD", saleRateNB: "13.21 ", purchaseRateNB: "13.21"},
    {num: "3", currency: "CZK", saleRateNB: "0.67", purchaseRateNB: "0.67"},
    {num: "4", currency: "DKK", saleRateNB: "2.52 ", purchaseRateNB: "2.52"}
];

$('body').append('<table></table>');

$('body > table').append(
    '<tr><th>Base currency: UAH</th><th>Currency</th><th>Sale Rate NB</th><th>Purchase Rate NB</th></tr>'
)

data.forEach(function(item, i, arr){
    $('body > table').append(
        '<tr><td>' + (i + 1) + '</td><td>' + item.currency + '</td><td>' + item.saleRateNB + '</td><td>' + item.purchaseRateNB + '</td></tr>'
    )
})
*/


